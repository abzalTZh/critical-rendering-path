import { validate } from './email-validator';

class SectionCreater {
  create (type) {
    if (type === 'standard') {
      const headerContent = document.createTextNode('Join Our Program');
      const formButtonText = document.createTextNode('Subscribe');

      return new Section(headerContent, formButtonText).constructSection();
    }

    if (type === 'advanced') {
      const headerContent = document.createTextNode('Join Our Advanced Program');
      const formButtonText = document.createTextNode('Subscribe to Advanced Program');

      return new Section(headerContent, formButtonText).constructSection();
    }
  }
}

class Section {
    constructor (headerContent, formButtonText) {
            this.headerContent = headerContent;
            this.formButtonText = formButtonText;
    }

    constructSection () {
        const learnMore = document.querySelector('section.app-section.app-section--image-culture');

        const joinSection = document.createElement('section');
        joinSection.className = 'app-section app-section--image-join';
        learnMore.parentNode.insertBefore(joinSection, learnMore.nextSibling);

        const sectionHeader = document.createElement('h2');
        sectionHeader.className = 'app-title';
    
        sectionHeader.appendChild(this.headerContent);
        joinSection.appendChild(sectionHeader);
    
        const sectionSubtitle = document.createElement('h3');
        sectionSubtitle.className = 'app-subtitle';
        const subtitleContent1 = document.createTextNode('Sed do eiusmod tempor incididunt');
        const subtitleContent2 = document.createTextNode('ut labore et dolore magna aliqua.');
    
        sectionSubtitle.appendChild(subtitleContent1);
        sectionSubtitle.appendChild(document.createElement('br'));
        sectionSubtitle.appendChild(subtitleContent2);
        joinSection.appendChild(sectionSubtitle);
    
        const formContainer = document.createElement('form');
        formContainer.className = 'app-form';
        const emailForm = document.createElement('input');
        emailForm.className = 'app-form__email-input';
        emailForm.setAttribute('type', 'email');
        emailForm.setAttribute('name', 'email');
        emailForm.setAttribute('placeholder', 'Email');
    
        joinSection.appendChild(formContainer);
        formContainer.appendChild(emailForm);
    
        const formButton = document.createElement('button');
        formButton.className = 'app-section__button app-section__button--subscribe';
        formButton.appendChild(this.formButtonText);
    
        formContainer.appendChild(formButton);
    }
}

export { SectionCreater };