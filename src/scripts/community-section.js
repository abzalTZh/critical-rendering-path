class SectionCreater2 {
    create() {
        return new CommunitySection().constructSection();
    }
}

class CommunitySection {
    constructSection() {
        const parentSection = document.querySelector('#sectionHeadline');

        const commSection = document.createElement('section');
        commSection.classList.add('app-section');
        commSection.classList.add('app-section--community');
        parentSection.parentNode.insertBefore(commSection, parentSection.nextSibling);

        const sectionHeader = document.createElement('h2');
        sectionHeader.className = 'app-title';
        const headerContent1 = document.createTextNode('Big Community of');
        const headerContent2 = document.createTextNode('People Like You');

        sectionHeader.appendChild(headerContent1);
        sectionHeader.appendChild(document.createElement('br'));
        sectionHeader.appendChild(headerContent2);
        commSection.appendChild(sectionHeader);

        const sectionSubtitle = document.createElement('h3');
        sectionSubtitle.className = 'app-subtitle';
        const subtitleContent1 = document.createTextNode('We’re proud of our products, and we’re really excited when');
        const subtitleContent2 = document.createTextNode('we get feedback from our users.');

        sectionSubtitle.appendChild(subtitleContent1);
        sectionSubtitle.appendChild(document.createElement('br'));
        sectionSubtitle.appendChild(subtitleContent2);
        commSection.appendChild(sectionSubtitle);

        const usersContainer = document.createElement('div');
        usersContainer.classList.add('app-section__users-container');
        commSection.appendChild(usersContainer);

        const users = [
            document.createElement('div'),
            document.createElement('div'),
            document.createElement('div')
        ]

        for (const user of users) {
            user.classList.add('app-section__user');
            usersContainer.appendChild(user);
        }

        const avis = [
            document.createElement('img'),
            document.createElement('img'),
            document.createElement('img'),
        ];

        for (const avi of avis) {
            avi.classList.add('app-section__user-avi');
        }

        const descs = [
            document.createElement('p'),
            document.createElement('p'),
            document.createElement('p')
        ];

        for (const desc of descs) {
            desc.classList.add('app-section__user-desc');
        }

        const names = [
            document.createElement('p'),
            document.createElement('p'),
            document.createElement('p')
        ];

        for (const name of names) {
            name.classList.add('app-section__user-name');
        }

        const positions = [
            document.createElement('p'),
            document.createElement('p'),
            document.createElement('p')
        ]

        for (const pos of positions) {
            pos.classList.add('app-section__user-pos');
        }

        for (let i = 0; i < 3; i++) {
            users[i].appendChild(avis[i]);
            users[i].appendChild(descs[i]);
            users[i].appendChild(names[i]);
            users[i].appendChild(positions[i]);
        }

        fetch('http://localhost:3000/community')
            .then((response) => response.json())
            .then((data) => {
                for (let i = 0; i < 3; i++) {
                    avis[i].setAttribute('src', data[i].avatar);
                    avis[i].setAttribute('alt', `${data[i].firstName} ${data[i].lastName} profile photo`);
                    names[i].textContent = `${data[i].firstName} ${data[i].lastName}`;
                    positions[i].textContent = data[i].position;
                }

                descs[0].textContent = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.';
                descs[1].textContent = 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.';
                descs[2].textContent = 'Aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.';
            })
            .catch((err) => console.log(err));
    }
}

export { SectionCreater2 };